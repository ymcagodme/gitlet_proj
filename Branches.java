import java.io.*;
import java.util.Set;
import java.util.HashSet;

public class Branches implements Serializable {

    private static final long serialVersionUID = 123456789L;
    private static Branches instance;  // Apply Singleton Design Pattern

    private String currrentBranch;
    private Set<String> branches;  // {branchNames}

    // Note that it's a "private" constructor because we are applying Singleton.
    private Branches() {
        instance = null;
        currrentBranch = null;
        branches = new HashSet<String>();
    }

    public static Branches getInstance() {
        if (instance == null && (instance = loadBranches()) == null) {
            instance = new Branches();
        }

        return instance;
    }

    public Set<Branch> all() {
        Set<Branch> allBranches = new HashSet<Branch>();
        for (String branchName : branches) {
            allBranches.add(Branch.loadBranch(branchName));
        }
        return allBranches;
    }

    public Branch find(String branchName) {
        if (branches.contains(branchName)) {
            return Branch.loadBranch(branchName);
        }
        return null;
    }

    public void setCurrentBranch(Branch b) {
        if (branches.contains(b.getName())) {
            currrentBranch = b.getName();
        }
    }

    public Branch getCurrentBranch() {
        return find(currrentBranch);
    }

    public void add(Branch b) {
        branches.add(b.getName());
    }

    public void remove(Branch b) {
        branches.remove(b.getName());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("=====================\n");
        sb.append("===== Branches ======\n");
        sb.append("=====================\n");
        sb.append("Current Branch: " + currrentBranch + "\n\n");
        sb.append("All Branches: \n");
        for (Branch b : all()) {
            sb.append("    - " + b.getName());
        }
        return sb.toString();
    }

    /**
     * -------------- Utility Methods -------------------
     */
    public void save() {
        try {
            String GITLET_DIR = GitletUtils.GITLET_DIR;
            String branchesPath = GITLET_DIR + "branches.ser";
            File branchesFile = new File(branchesPath);
            FileOutputStream fileOut = new FileOutputStream(branchesFile);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(this);
        } catch (IOException e) {
            String msg = "IOException while saving Branches.";
            System.out.println(msg);
        }
    }

    protected static Branches loadBranches() {
        Branches myBranches = null;
        String GITLET_DIR = GitletUtils.GITLET_DIR;
        String branchesPath = GITLET_DIR + "branches.ser";
        File branchesFile = new File(branchesPath);
        if (branchesFile.exists()) {
            try {
                FileInputStream fileIn = new FileInputStream(branchesFile);
                ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                myBranches = (Branches) objectIn.readObject();
            } catch (IOException e) {
                String msg = "IOException while loading Branches.";
                System.out.println(msg);
            } catch (ClassNotFoundException e) {
                String msg = "ClassNotFoundException while loading Branches.";
                System.out.println(msg);
            }
        }
        return myBranches;
    }
}
