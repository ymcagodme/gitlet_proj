// import static java.nio.file.StandardCopyOption.*;
// import java.nio.file.Files;
// import java.nio.file.Path;
import java.io.*;
import java.nio.channels.FileChannel;
import java.util.Arrays;

public class TestUtils {
    public static void main (String[] args) {
        String command = args[0];
        switch (command) {
            case "commit":
                printCommit(args[1]);
                break;
            case "branch":
                printBranch(args[1]);
                break;
            case "branches":
                printBranches();
                break;
            case "delete":
                deleteCommit(args[1]);
                break;
            case "printCommit":
                printSubDir();
                break;
        }
    }

    public static void deleteCommit(String commitId) {
        String oldPath = GitletUtils.GITLET_DIR + "commits/" + commitId + "/";
        System.out.println("Deleting path: " + oldPath);
        GitletUtils.recursiveDelete(new File(oldPath));
    }

    public static void printBranch(String branchName) {
        Branch b = Branch.loadBranch(branchName);
        System.out.println("======================================");
        System.out.println(b);
        System.out.println("======================================");
    }

    public static void printCommit(String commitId) {
        Commit c = Commit.loadCommit(commitId);
        System.out.println("======================================");
        System.out.println(c);
        System.out.println("----- old files ----- \n" + c.getTrackingFiles());
        System.out.println("----- staged files ----- \n" + c.getStagedFiles());
        System.out.println("----- parent ----- \n" + c.getParent());
        System.out.println("======================================");
    }

    public static void printBranches() {
        Branches bs = Branches.loadBranches();
        Branch b = bs.getCurrentBranch();
        System.out.println("================== Current Branch ====================");
        System.out.println(b);
        System.out.println("======================================================");
        System.out.println("");
    }

    public static void printSubDir() {
        String path = GitletUtils.GITLET_DIR + "commits/";
        File file = new File(path);
        System.out.println("checking Dir under " + path);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        System.out.println(Arrays.toString(directories));
    }
}
