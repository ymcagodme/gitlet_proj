import java.io.*;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;
import java.util.Scanner;

public class Gitlet {

    // private static final String GITLET_DIR = ".gitlet/";
    private static String GITLET_DIR = GitletUtils.GITLET_DIR;

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Please enter a command.");
            return;
        }

        String command  = args[0];
        if (!command.equals("init") && isGitletExisted() == false) {
            System.out.println("Please Gitlet init first.");
            System.exit(0);
        }

        switch (command) {
            case "init":
                init();
                break;
            case "add":
                checkArgsLength(args, 2);
                add(args[1]);
                break;
            case "commit":
                if (args.length < 2) {
                    System.out.println("Please enter a commit message.");
                } else {
                    commit(args[1]);
                }
                break;
            case "rm":
                checkArgsLength(args, 2);
                rm(args[1]);
                break;
            case "log":
                log();
                break;
            case "global-log":
                globalLog();
                break;
            case "find":
                checkArgsLength(args, 2);
                find(args[1]);
                break;
            case "branch":
                checkArgsLength(args, 2);
                branch(args[1]);
                break;
            case "status":
                status();
                break;
            case "checkout":
                checkArgsLength(args, 2);
                checkout(args);
                break;
            case "rm-branch":
                checkArgsLength(args, 2);
                rmBranch(args[1]);
                break;
            // case "reset":
            //     checkArgsLength(args, 2);
            //     reset(args[1]);
            //     break;
            // case "merge":
            //     checkArgsLength(args, 2);
            //     merge(args[1]);
            //     break;
            // case "rebase":
            //     checkArgsLength(args, 2);
            //     rebase(args[1]);
            //     break;
            // case "i-rebase":
            //     checkArgsLength(args, 2);
            //     iRebase(args[1]);
            //     break;
            default:
                System.out.println("Unsupport command");
        }
    }


    /**
     * ========= Command methods ==========
     */

    private static void init() {
        File f = new File(GITLET_DIR + "branches/");
        if (f.exists()) {
            String errorMsg = "A gitlet version control system already exists "
                            + "in the current directory.";
            System.out.println(errorMsg);
            return;
        }

        f.mkdirs();
        f = new File(GITLET_DIR + "commits/");
        f.mkdirs();

        // Create a initial commit and master branch
        Branches branches = Branches.getInstance();
        Commits commits = Commits.getInstance();

        Commit initCommit = new Commit(null);
        initCommit.commit("initial commit");
        commits.add(initCommit);
        commits.save();

        Branch masterBranch = new Branch("master", initCommit);
        masterBranch.save();

        branches.add(masterBranch);
        branches.setCurrentBranch(masterBranch);
        branches.save();
    }

    private static void add(String fileName) {
        // Note: we are not saving the file; instead, we perform validation.
        File targetFile = new File(fileName);

        // Check if the input filename exist
        if (!targetFile.exists()) {
            System.out.println("File does not exist.");
            return;
        }

        // Add the file into the tmp commit and save
        Commit tmpHead = getTmpHead();
        tmpHead.addFile(fileName);
        tmpHead.save();
    }

    private static void commit(String commitMsg) {
        Branch currentBranch = getCurrentBranch();
        Commit tmpHead = currentBranch.getTmpHead();

        // Check if any new added files
        if (tmpHead.getStagedFiles().size() == 0) {
            System.out.println("No changes added to the commit.");
            return;
        }

        currentBranch.commit(commitMsg);
        currentBranch.save();

        // Moving files
        Commit head = currentBranch.getHead();
        String commitFolder = GITLET_DIR + "commits/" + head.getCommitId()+"/";
        for (String fileName : head.getStagedFiles()) {
            File src = new File(fileName);
            File dst = new File(commitFolder + fileName);
            try {
                GitletUtils.copyFile(src, dst);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void rm(String fileName) {
        Commit tmpHead = getTmpHead();
        try {
            tmpHead.removeFile(fileName);
            tmpHead.save();
        } catch (Exception e) {
            System.out.println("No reason to remove the file.");
        }
    }

    private static void status() {
        Branches branches = Branches.getInstance();
        Branch currentBranch = branches.getCurrentBranch();
        Commit tmpHead = currentBranch.getTmpHead();
        StringBuilder sb = new StringBuilder();
        sb.append("=== Branches ===\n");
        for (Branch b : branches.all()) {
            if (b.getName().equals(currentBranch.getName())) {
                sb.append("*" + b.getName() + "\n");
            } else {
                sb.append(b.getName() + "\n");
            }
        }
        sb.append("\n=== Staged Files ===\n");
        for (String fileName : tmpHead.getStagedFiles()) {
            sb.append(fileName + "\n");
        }

        sb.append("\n=== Files Marked for Removal ===\n");
        for (String fileName : tmpHead.getRemovalFiles()) {
            sb.append(fileName + "\n");
        }
        System.out.println(sb);
    }

    private static void log() {
        Commit head = getHead();
        StringBuilder sb = new StringBuilder();
        sb.append("====\n");
        sb.append(head.toString());
        head = head.getParent();
        while (head != null) {
            sb.append("\n\n====\n");
            sb.append(head.toString());
            head = head.getParent();
        }
        System.out.println(sb);
    }

    private static void globalLog() {
        for (Commit c : Commits.getInstance().all()) {
            System.out.println("====");
            System.out.println(c);
            System.out.println("");
        }
    }

    private static void find(String msg) {
        Commit head = getHead();
        while (head != null) {
            if (head.getCommitMsg().equals((msg))) {
                System.out.println(head.getCommitId());
                return;
            }
            head = head.getParent();
        }
        System.out.println("Found no commit with that message.");
    }

    private static void checkout(String[] args) {
        // Case 1: only given one argument.
        if (args.length == 2) {
            // Either checkout file or branch (branch take precedence)
            String fileOrBranchName = args[1];
            String errorMsg = "File does not exist in the most recent commit"
                            + ", or no such branch exists.";
            try {
                // We first try to checkout as a branch name
                checkoutBranch(args[1]);
            } catch (Exception be) {
                // If can't find the branch with the given name, which trigger an exception.
                // We then try to checkout as a file name
                try {
                    checkoutFile(args[1]);
                } catch (Exception fe) {
                    System.out.println(errorMsg);
                }
            }
        // Case 2: given two arguments.
        } else {
            try {
                checkoutFileInCommit(args[1], args[2]);
            } catch (Exception fce) {
                System.out.println(fce.getMessage());
            }
        }
    }

    private static void checkoutFileInCommit(String commitId, String fileName) throws Exception {
        Commit head = getHead();
        // Locate the commit in the current branch
        while (!head.getCommitId().equals(commitId)) {
            head = head.getParent();
            if (head == null) {
                throw new Exception("No commit with that id exists.");
            }
        }

        // Check if the file in the tracking file list.
        Set<String> stagedFiles = head.getStagedFiles();
        Set<String> trackingFiles = head.getTrackingFiles();
        if (!trackingFiles.contains(fileName) && !stagedFiles.contains(fileName)) {
            throw new Exception("File does not exist in that commit.");
        }

        promptUser();
        if (stagedFiles.contains(fileName)) {
            copyFileFromCommitId(head.getCommitId(), fileName, false);
        } else {
            while (head != null) {
                if (head.getStagedFiles().contains(fileName)) {
                    copyFileFromCommitId(head.getCommitId(), fileName, false);
                    return;
                }
                head = head.getParent();
            }
        }
    }

    private static void checkoutFile(String fileName) throws Exception {
        Commit tmpHead = getTmpHead();
        // Check if the file exist in the tracking file list
        Set<String> trackingFiles = tmpHead.getTrackingFiles();
        if (!trackingFiles.contains(fileName)) {
            throw new Exception("file not found");
        }

        promptUser();
        Commit head = getHead();
        while (head != null) {
            Set<String> stagedFiles = head.getStagedFiles();
            if (stagedFiles.contains(fileName)) {
                copyFileFromCommitId(head.getCommitId(), fileName, false);
                break;
            }
            head = head.getParent();
        }
    }

    private static void checkoutBranch(String branchName) throws Exception {
        Branches branches = Branches.getInstance();
        Branch b = branches.find(branchName);
        if (b == null) {
            throw new Exception("branch not found");
        }

        promptUser();
        branches.setCurrentBranch(b);
        branches.save();

        Commit head = getHead();
        Stack<Commit> chain = getCommitChainFromTop(head);
        while (!chain.empty()) {
            head = chain.pop();
            for (String fileName : head.getStagedFiles()) {
                copyFileFromCommitId(head.getCommitId(), fileName, false);
            }
        }
    }

    private static void branch(String branchName) {
        Branches branches = Branches.getInstance();
        if (branches.find(branchName) != null) {
            System.out.println("A branch with that name already exists.");
            return;
        }

        Commit currentHead = getHead();
        Branch newBranch = new Branch(branchName, currentHead);
        newBranch.save();

        branches.add(newBranch);
        branches.save();
    }

    private static void rmBranch(String branchName) {
        Branches branches = Branches.getInstance();
        Branch targetBranch = branches.find(branchName);
        Branch currentBranch = getCurrentBranch();
        if (targetBranch == null) {
            System.out.println("A branch with that name does not exist.");
            return;
        } else if (currentBranch.equals(targetBranch)) {
            System.out.println("Cannot remove the current branch.");
            return;
        }
        branches.remove(targetBranch);
        branches.save();
    }

    // private static void reset(String commitId) {
    //     Set<String> allCommitIds = Branches.loadBranches().getAllCommitIds();
    //     if (!allCommitIds.contains(commitId)) {
    //         System.out.println("No commit with that id exists.");
    //         return;
    //     }
    //     promptUser();
    //     Branch currentBranch = getCurrentBranch();
    //     currentBranch.setHead(commitId);
    // }

    // private static void merge(String branchName) {
    //     Branches branches = Branches.loadBranches();
    //     Branch currentBranch = branches.getCurrentBranch();
    //     Set<String> allBranches = branches.getAllBranchNames();
    //     if (currentBranch.getName().equals(branchName)) {
    //         System.out.println("Cannot merge a branch with itself.");
    //         return;
    //     } else if (!allBranches.contains(branchName)) {
    //         System.out.println("A branch with that name does not exist.");
    //         return;
    //     }
    //     promptUser();

    //     Branch otherBranch = Branch.loadBranch(branchName);
    //     Commit currentHead = currentBranch.getHead();
    //     Commit otherHead   = otherBranch.getHead();
    //     Commit splitCommit = getCommonAncestor(currentHead, otherHead);

    //     Stack<Commit> path = getPath(otherHead, splitCommit);
    //     Commit currentTmp = getTmpHead();
    //     HashMap<String, String> currentFiles = currentTmp.getTrackingFiles();
    //     for (String key : currentTmp.getStagedFiles().keySet()) {
    //         currentFiles.put(key, currentTmp.getStagedFiles().get(key));
    //     }
    //     boolean isModified = false;
    //     while (!path.empty()) {
    //         Commit stepCommit = path.pop();
    //         HashMap<String, String> stagedFiles = stepCommit.getStagedFiles();
    //         for (String fileName : stagedFiles.keySet()) {
    //             // Case 1: the file is not in the current branch
    //             if (!currentFiles.containsKey(fileName)) {
    //                 copyFileFromCommitId(stepCommit.getCommitId(), fileName, false);
    //                 try {
    //                     currentTmp.addFile(fileName, GitletUtils.getMD5Checksum(fileName));
    //                 } catch (Exception e) {
    //                     e.printStackTrace();
    //                 }
    //             } else {
    //                 String currentMD5 = currentFiles.get(fileName);
    //                 String otherMD5 = stagedFiles.get(fileName);
    //                 if (!currentMD5.equals(otherMD5)) {
    //                     copyFileFromCommitId(stepCommit.getCommitId(), fileName, true);
    //                     try {
    //                         fileName += ".conflicted";
    //                         currentTmp.addFile(fileName, GitletUtils.getMD5Checksum(fileName));
    //                     } catch (Exception e) {
    //                         e.printStackTrace();
    //                     }
    //                 }
    //             }
    //             isModified = true;
    //         }
    //     }
    //     if (isModified) {
    //         currentBranch.commit("merge with " + branchName);
    //     }
    // }

    // private static void rebase(String branchName) {
    //     Branches branches = Branches.loadBranches();
    //     Branch currentBranch = branches.getCurrentBranch();
    //     Commit currentHead = getHead();
    //     Branch targetBranch;
    //     Commit targetHead;
    //     Set<String> allBranches = branches.getAllBranchNames();
    //     if (currentBranch.getName().equals(branchName)) {
    //         System.out.println("Cannot rebase a branch onto itself.");
    //         return;
    //     } else if (!allBranches.contains(branchName)) {
    //         System.out.println("A branch with that name does not exist.");
    //         return;
    //     } else {
    //         targetBranch = Branch.loadBranch(branchName);
    //         targetHead = targetBranch.getHead();
    //         if (currentHead.equals(targetHead)) {
    //             System.out.println("Already up-to-date.");
    //             return;
    //         }
    //     }
    //     promptUser();

    //     Commit splitCommit = getCommonAncestor(currentHead, targetHead);
    //     if (splitCommit.equals(currentHead)) {
    //         currentBranch.setHead(targetHead.getCommitId());
    //         return;
    //     }

    //     Commit currentTmp = getTmpHead();
    //     HashMap<String, String> currentFiles = currentTmp.getTrackingFiles();
    //     for (String key : currentTmp.getStagedFiles().keySet()) {
    //         currentFiles.put(key, currentTmp.getStagedFiles().get(key));
    //     }
    //     Stack<Commit> targetPath = getPath(targetHead, splitCommit);
    //     while (!targetPath.empty()) {
    //         Commit stepCommit = targetPath.pop();
    //         HashMap<String, String> stagedFiles = stepCommit.getStagedFiles();
    //         for (String fileName : stagedFiles.keySet()) {
    //             // Case 1: the file is not in the current branch
    //             if (!currentFiles.containsKey(fileName)) {
    //                 copyFileFromCommitId(stepCommit.getCommitId(), fileName, false);
    //                 try {
    //                     currentTmp.addFile(fileName, GitletUtils.getMD5Checksum(fileName));
    //                 } catch (Exception e) {
    //                     e.printStackTrace();
    //                 }
    //             } else {
    //                 String currentMD5 = currentFiles.get(fileName);
    //                 String otherMD5 = stagedFiles.get(fileName);
    //                 if (!currentMD5.equals(otherMD5)) {
    //                     copyFileFromCommitId(stepCommit.getCommitId(), fileName, true);
    //                     try {
    //                         fileName += ".conflicted";
    //                         currentTmp.addFile(fileName, GitletUtils.getMD5Checksum(fileName));
    //                     } catch (Exception e) {
    //                         e.printStackTrace();
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     Stack<Commit> currentPath = getPath(currentHead, splitCommit);
    //     Commit replayedCommit = targetHead;
    //     while (!currentPath.empty()) {
    //         Commit t = currentPath.pop();
    //         Commit newCommit = new Commit(replayedCommit.getCommitId());
    //         newCommit.commit(t.getCommitMsg());
    //         replayedCommit = newCommit;
    //     }
    //     currentBranch.setHead(replayedCommit.getCommitId());
    // }

    // private static void iRebase(String branchName) {
    //     Branches branches = Branches.loadBranches();
    //     Branch currentBranch = branches.getCurrentBranch();
    //     Commit currentHead = getHead();
    //     Branch targetBranch;
    //     Commit targetHead;
    //     Set<String> allBranches = branches.getAllBranchNames();
    //     if (currentBranch.getName().equals(branchName)) {
    //         System.out.println("Cannot rebase a branch onto itself.");
    //         return;
    //     } else if (!allBranches.contains(branchName)) {
    //         System.out.println("A branch with that name does not exist.");
    //         return;
    //     } else {
    //         targetBranch = Branch.loadBranch(branchName);
    //         targetHead = targetBranch.getHead();
    //         if (currentHead.equals(targetHead)) {
    //             System.out.println("Already up-to-date.");
    //             return;
    //         }
    //     }
    //     promptUser();

    //     Commit splitCommit = getCommonAncestor(currentHead, targetHead);
    //     if (splitCommit.equals(currentHead)) {
    //         currentBranch.setHead(targetHead.getCommitId());
    //         return;
    //     }

    //     Commit currentTmp = getTmpHead();
    //     HashMap<String, String> currentFiles = currentTmp.getTrackingFiles();
    //     for (String key : currentTmp.getStagedFiles().keySet()) {
    //         currentFiles.put(key, currentTmp.getStagedFiles().get(key));
    //     }
    //     Stack<Commit> targetPath = getPath(targetHead, splitCommit);
    //     while (!targetPath.empty()) {
    //         Commit stepCommit = targetPath.pop();
    //         HashMap<String, String> stagedFiles = stepCommit.getStagedFiles();
    //         for (String fileName : stagedFiles.keySet()) {
    //             // Case 1: the file is not in the current branch
    //             if (!currentFiles.containsKey(fileName)) {
    //                 copyFileFromCommitId(stepCommit.getCommitId(), fileName, false);
    //                 try {
    //                     currentTmp.addFile(fileName, GitletUtils.getMD5Checksum(fileName));
    //                 } catch (Exception e) {
    //                     e.printStackTrace();
    //                 }
    //             } else {
    //                 String currentMD5 = currentFiles.get(fileName);
    //                 String otherMD5 = stagedFiles.get(fileName);
    //                 if (!currentMD5.equals(otherMD5)) {
    //                     copyFileFromCommitId(stepCommit.getCommitId(), fileName, true);
    //                     try {
    //                         fileName += ".conflicted";
    //                         currentTmp.addFile(fileName, GitletUtils.getMD5Checksum(fileName));
    //                     } catch (Exception e) {
    //                         e.printStackTrace();
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     Stack<Commit> currentPath = getPath(currentHead, splitCommit);
    //     Commit replayedCommit = targetHead;
    //     Scanner sc = new Scanner(System.in);
    //     int initialSize = currentPath.size();
    //     while (!currentPath.empty()) {
    //         Commit t = currentPath.pop();
    //         System.out.println("Currently replaying:");
    //         System.out.println("====");
    //         System.out.println(t);

    //         String input;
    //         do {
    //             String promptMsg = "Would you like to (c)ontinue, (s)kip this commit, or change this commit's (m)essage?";
    //             System.out.println(promptMsg);
    //             input = sc.nextLine().trim();
    //             Commit newCommit;
    //             switch (input) {
    //                 case "c":
    //                     newCommit = new Commit(replayedCommit.getCommitId());
    //                     newCommit.commit(t.getCommitMsg());
    //                     replayedCommit = newCommit;
    //                     break;
    //                 case "s":
    //                     if (currentPath.size() == initialSize - 1 || currentPath.size() == 0) {
    //                         input = "fail";
    //                         break;
    //                     }
    //                     break;
    //                 case "m":
    //                     System.out.println("Please enter a new message for this commit.");
    //                     String newMsg = sc.nextLine();
    //                     newCommit = new Commit(replayedCommit.getCommitId());
    //                     newCommit.commit(newMsg);
    //                     replayedCommit = newCommit;
    //                     break;
    //             }
    //         } while(!input.equals("c") && !input.equals("s") && !input.equals("m"));
    //     }
    //     currentBranch.setHead(replayedCommit.getCommitId());
    // }


    /**
     *  ========= Utils ==========
    */
    private static Stack<Commit> getPath(Commit src, Commit dst) {
        Stack<Commit> path = new Stack<Commit>();
        while (src != null && !src.equals(dst)) {
            path.push(src);
            src = src.getParent();
        }
        return path;
    }
    private static Commit getCommonAncestor(Commit c1, Commit c2) {
        while (!c1.equals(c2)) {
            if (c1.getTimeStamp() > c2.getTimeStamp()) {
                c1 = c1.getParent();
            } else {
                c2 = c2.getParent();
            }
        }
        return c1;
    }

    private static Stack<Commit> getCommitChainFromTop(Commit head) {
        Stack<Commit> chain = new Stack<Commit>();
        while (head != null) {
            chain.push(head);
            head = head.getParent();
        }
        return chain;
    }
    private static void copyFileFromCommitId(String commitId, String fileName, boolean isConflicted) {
        File dst = new File(fileName);
        String srcPath = GITLET_DIR + "commits/" + commitId + "/" + dst.getName();
        File src = new File(srcPath);
        try {
            if (isConflicted) {
                dst = new File(fileName + ".conflicted");
            }
            GitletUtils.copyFile(src, dst);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void promptUser() {
        String msg = "Warning: The command you entered may alter the files in "
                   + "your directory. Uncommited changes may be lost. Are you "
                   + "sure you want to continue? (yes/no)";
        Scanner sc = new Scanner(System.in);
        System.out.println(msg);
        String action = sc.next();
        if (!action.equals("yes")) {
            System.out.println("Did not type 'yes', so aborting.");
            System.exit(0);
        }
    }

    private static void checkArgsLength(String[] args, int length) {
        if (args.length < length) {
            System.out.println("Did not enter enough arguments.");
            System.exit(0);
        }
    }

    private static boolean isGitletExisted() {
        File f = new File(GITLET_DIR);
        return f.exists();
    }

    private static Branch getCurrentBranch() {
        return Branches.getInstance().getCurrentBranch();
    }

    private static Commit getHead() {
        return getCurrentBranch().getHead();
    }

    private static Commit getTmpHead() {
        return getCurrentBranch().getTmpHead();
    }
}
