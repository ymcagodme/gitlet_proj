import java.io.*;
import java.util.Set;
import java.util.HashSet;

public class Branch implements Serializable {

    private static final long serialVersionUID = 123456789L;
    private String name;
    private String originHeadId;
    private String curHeadId;
    private String tmpHeadId;
    private Set<Commit> pendingToSave;

    public Branch(String name, Commit head) {
        this.name = name;
        this.originHeadId = head.getCommitId();
        this.curHeadId = head.getCommitId();
        this.pendingToSave = new HashSet<Commit>();

        // Create a temp head
        Commit tmp = new Commit(this.curHeadId);
        this.tmpHeadId = tmp.getCommitId();
        pendingToSave.add(tmp);
    }

    public String getName() {
        return name;
    }

    public Commit getHead() {
        return Commits.getInstance().find(curHeadId);
    }

    public Commit getTmpHead() {
        return Commit.loadCommit(tmpHeadId);
    }

    public void commit(String commitMsg) {
        Commit t = getTmpHead();
        t.commit(commitMsg);
        curHeadId = t.getCommitId();
        pendingToSave.add(t);

        Commit tmp = new Commit(curHeadId);
        tmpHeadId = tmp.getCommitId();
        pendingToSave.add(tmp);
    }

    @Override
    public String toString() {
        String retval = "Branch name: " + getName() + "\n";
        retval +=       "Head: " + curHeadId + "\n";
        retval +=       "tmpHead: " + tmpHeadId;
        return retval;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Branch) {
            Branch other = (Branch) o;
            if (this.getName().equals(other.getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * ========== Utils ===========
    */
    public void save() {
        try {
            for (Commit c : pendingToSave) {
                c.save();
            }
            pendingToSave.clear();

            String GITLET_DIR = GitletUtils.GITLET_DIR;
            String branchPath = GITLET_DIR + "branches/" + getName() + ".ser";
            File branchFile = new File(branchPath);
            FileOutputStream fileOut = new FileOutputStream(branchFile);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(this);
        } catch (IOException e) {
            String msg = "IOException while saving Branch.";
            System.out.println(msg);
        }
    }

    public static Branch loadBranch(String branchName) {
        Branch myBranch = null;
        String GITLET_DIR = GitletUtils.GITLET_DIR;
        String branchPath = GITLET_DIR + "branches/" + branchName + ".ser";
        File branchFile = new File(branchPath);
        if (branchFile.exists()) {
            try {
                FileInputStream fileIn = new FileInputStream(branchFile);
                ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                myBranch = (Branch) objectIn.readObject();
            } catch (IOException e) {
                String msg = "IOException while loading Branch.";
                System.out.println(msg);
            } catch (ClassNotFoundException e) {
                String msg = "ClassNotFoundException while loading Branch.";
                System.out.println(msg);
            }
        }
        return myBranch;
    }
}
