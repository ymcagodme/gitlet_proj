import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;

/**
 * Class that provides JUnit tests for Gitlet, as well as a couple of utility
 * methods.
 *
 * @author Joseph Moghadam
 *
 *         Some code adapted from StackOverflow:
 *
 *         http://stackoverflow.com/questions
 *         /779519/delete-files-recursively-in-java
 *
 *         http://stackoverflow.com/questions/326390/how-to-create-a-java-string
 *         -from-the-contents-of-a-file
 *
 *         http://stackoverflow.com/questions/1119385/junit-test-for-system-out-
 *         println
 *
 */
public class GitletPublicTest {
    private static final String GITLET_DIR = ".gitlet/";
    private static final String TESTING_DIR = "test_files/";

    /* matches either unix/mac or windows line separators */
    private static final String LINE_SEPARATOR = "\r\n|[\r\n]";

    /**
     * Deletes existing gitlet system, resets the folder that stores files used
     * in testing.
     *
     * This method runs before every @Test method. This is important to enforce
     * that all tests are independent and do not interact with one another.
     */
    @Before
    public void setUp() {
        File f = new File(GITLET_DIR);
        if (f.exists()) {
            recursiveDelete(f);
        }
        f = new File(TESTING_DIR);
        if (f.exists()) {
            recursiveDelete(f);
        }
        f.mkdirs();
    }

    /**
     * Tests that init creates a .gitlet directory. Does NOT test that init
     * creates an initial commit, which is the other functionality of init.
     */
    // @Ignore
    @Test
    public void testBasicInitialize() {
        gitlet("init");
        File f = new File(GITLET_DIR);
        assertTrue(f.exists());
    }

    // @Ignore
    @Test
    public void testInitIfExisted() {
        gitlet("init");
        String stdout = gitlet("init");
        String errorMsg = "A gitlet version control system already exists "
                        + "in the current directory.\n";
        assertEquals(errorMsg, stdout);
    }

    // @Ignore
    @Test
    public void testBasicBranch() {
        gitlet("init");
        String stdout = gitlet("branch", "master");
        assertTrue(stdout.contains("A branch with that name already exists."));
        gitlet("branch", "dev");
        stdout = gitlet("status");
        assertTrue(stdout.contains("dev"));
    }

    /**
     * Start with one commit: a commit that contains no files and has the
     * commit message initial commit.
    */
    // @Ignore
    @Test
    public void testInitWithInitCommit() {
        gitlet("init");
        String commitMessage1 = "initial commit";

        String logContent = gitlet("log");
        // System.out.println(logContent);
        assertArrayEquals(new String[] { commitMessage1 },
                extractCommitMessages(logContent));
    }

    // @Ignore
    @Test
    public void testBasicAdd() {
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("init");
        String stdout = gitlet("add", "ThisIsWrongName.txt");
        assertTrue(stdout.contains("File does not exist."));

        gitlet("add", wugFileName);
        stdout = gitlet("status");
        assertTrue(stdout.contains(wugFileName));
    }

    /**
     * If the file had been marked for removal, instead just unmark it.
    */
    // @Ignore
    @Test
    public void testAddWithRemoval() {
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("init");
        gitlet("add", wugFileName);
        gitlet("rm", wugFileName);
        String stdout = gitlet("status");
        assertTrue(!stdout.contains(wugFileName));
    }


    /**
     * If the file has not been modified since the last commit, aborts and
     * prints the error message
    */
    // @Ignore
    @Test
    public void testAddWithAndWithoutModified() throws InterruptedException {
        gitlet("init");
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        gitlet("commit", "This is second commit.");
        String stdout = gitlet("add", wugFileName);
        assertTrue(stdout.contains("File has not been modified since the last commit."));
        stdout = gitlet("status");
        assertTrue(!stdout.contains(wugFileName));

        // writeFile(wugFileName, "Modified!");
        // stdout = gitlet("add", wugFileName);
        // stdout = gitlet("status");
        // assertTrue(stdout.contains(wugFileName));
    }

    // @Ignore
    @Test
    public void testBasicCommit() {
        gitlet("init");

        String stdout = gitlet("commit");
        assertTrue(stdout.contains("Please enter a commit message."));

        stdout = gitlet("commit", "this is the second commit");
        assertTrue(stdout.contains("No changes added to the commit."));

        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        gitlet("commit", "This is second commit.");
        stdout = gitlet("log");
        assertTrue(stdout.contains("This is second commit."));
    }

    // @Ignore
    @Test
    public void testBasicRemove() {
        gitlet("init");
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        String stdout = gitlet("status");
        assertTrue(stdout.contains(wugFileName));
        gitlet("rm", wugFileName);
        stdout = gitlet("status");
        assertTrue(!stdout.contains(wugFileName));

        gitlet("add", wugFileName);
        gitlet("commit", "added wug");
        stdout = gitlet("log");
        stdout = gitlet("rm", wugFileName);
        stdout = gitlet("status");
        assertTrue(stdout.contains(wugFileName));
    }

    // @Ignore
    @Test
    public void testBasicGlobalLog() {
        // Test with more than once branch
        gitlet("init");
        String commitMessage1 = "initial commit";
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        String commitMessage2 = "added wug.txt";
        gitlet("commit", commitMessage2);

        // Checkout dev branch and make a commit
        gitlet("branch", "dev");
        gitlet("checkout", "dev");
        writeFile(wugFileName, "modified in the other branch");
        gitlet("add", wugFileName);
        String commitMessage3 = "modified wug.txt";
        gitlet("commit", commitMessage3);

        // Checkout back to master and make another commit
        gitlet("checkout", "master");
        writeFile(wugFileName, "modified in master branch");
        gitlet("add", wugFileName);
        String commitMessage4 = "modified wug.txt in master branch";
        gitlet("commit", commitMessage4);

        String stdout;
        stdout = gitlet("global-log");
        // System.out.println(stdout);
        assertTrue(stdout.contains(commitMessage3));
    }

    // @Ignore
    @Test
    public void testBasicFind() {
        gitlet("init");
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        gitlet("commit", "added wug.txt");

        String wtwFileName = TESTING_DIR + "wtw.txt";
        String wtwText = "This is a wtw.";
        createFile(wtwFileName, wtwText);
        gitlet("add", wtwFileName);
        gitlet("commit", "added wtw.txt");

        Branches branches = Branches.loadBranches();
        Commit head = branches.getCurrentBranch().getHead();
        String stdout = gitlet("find", "added wug.txt");
        assertTrue(stdout.contains(head.getParent().getCommitId()));
    }

    /**
     * Tests that checking out a file name will restore the version of the file
     * from the previous commit. Involves init, add, commit, and checkout.
     */
    // @Ignore
    @Test
    public void testBasicCheckout() {
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("init");
        gitlet("add", wugFileName);
        gitlet("commit", "added wug");
        writeFile(wugFileName, "This is not a wug.");
        gitlet("checkout", wugFileName);
        assertEquals(wugText, getText(wugFileName));
    }

    // @Ignore
    @Test
    public void testCheckoutFunctionality() {
        gitlet("init");
        String stdout;

        // Commit A
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("init");
        gitlet("add", wugFileName);
        gitlet("commit", "added wug");

        // Commit B
        String otherFileName = TESTING_DIR + "other.txt";
        String otherText = "This is other file";
        createFile(otherFileName, otherText);
        gitlet("add", otherFileName);
        gitlet("commit", "added other.txt");

        // Commit C (Split point)
        String wugTextAtC = "I am at commit C (Split point)";
        writeFile(wugFileName, wugTextAtC);
        gitlet("add", wugFileName);
        gitlet("commit", "modified wug.txt @ commit C (Split)");

        // Create and switch to the branch "dev"
        gitlet("branch", "dev");
        stdout = gitlet("checkout", "dev");
        // Should prompt users
        assertTrue(stdout.contains("yes/no"));

        // Commit D (in dev branch)
        String wugTextAtD = "I am at commit D (dev branch)";
        writeFile(wugFileName, wugTextAtD);
        gitlet("add", wugFileName);
        gitlet("commit", "modified wug.txt in dev branch (D)");

        // Checkout master branch and make commit E
        stdout = gitlet("checkout", "master");
        String wugTextAtE = "I am at commit E (master branch)";
        writeFile(wugFileName, wugTextAtE);
        gitlet("add", wugFileName);
        gitlet("commit", "modified wug.txt in master branch (E)");
        assertEquals(wugTextAtE, getText(wugFileName));

        // Checkout dev branch should change wug.txt content
        gitlet("checkout", "dev");
        assertEquals(wugTextAtD, getText(wugFileName));
    }

    // @Ignore
    @Test
    public void testCheckoutFailure() {
        gitlet("init");
        // checkout dev branch which is not existed
        String stdout = gitlet("checkout", "dev");
        String expectedMsg = "File does not exist in the most recent commit"
                           + ", or no such branch exists.";
        assertTrue(stdout.contains(expectedMsg));

        // checkout a file which is not in the previous commit
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("init");
        gitlet("add", wugFileName);
        gitlet("commit", "added wug");
        Commit oldCommit = Branches.loadBranches().getCurrentBranch().getHead();
        stdout = gitlet("checkout", wugFileName + ".non");
        assertTrue(stdout.contains(expectedMsg));

        // If no commit with the given id exists
        stdout = gitlet("checkout", "123", wugFileName);
        assertTrue(stdout.contains("No commit with that id exists."));

        // if the file does not exist in the given commit
        writeFile(wugFileName, "modified once");
        gitlet("add", wugFileName);
        gitlet("commit", "modified wug.txt");
        String otherFileName = TESTING_DIR + "other.txt";
        String otherText = "This is other file";
        createFile(otherFileName, otherText);
        gitlet("add", otherFileName);
        gitlet("commit", "added other.txt");
        stdout = gitlet("checkout", oldCommit.getCommitId(), otherFileName);
        assertTrue(stdout.contains("File does not exist in that commit."));

        // if the file is no longer being tracked, can't restore form
        // the most recent commit
        gitlet("rm", wugFileName);
        gitlet("commit", "remove wug.txt");
        stdout = gitlet("checkout", wugFileName);
        assertTrue(stdout.contains(expectedMsg));
    }

    // @Ignore
    @Test
    public void testStatusBasic() {
        gitlet("init");
        gitlet("branch", "dev");
        String stdout = gitlet("status");
        assertTrue(stdout.contains("*master"));
        assertTrue(!stdout.contains("*dev"));

        gitlet("checkout", "dev");
        stdout = gitlet("status");
        assertTrue(!stdout.contains("*master"));
        assertTrue(stdout.contains("*dev"));
    }

    /**
     * Tests that log prints out commit messages in the right order. Involves
     * init, add, commit, and log.
     */
    // @Ignore
    @Test
    public void testBasicLog() {
        gitlet("init");
        String commitMessage1 = "initial commit";

        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        String commitMessage2 = "added wug";
        gitlet("commit", commitMessage2);

        writeFile(wugFileName, "modified");
        String stdout = gitlet("add", wugFileName);
        String commitMessage3 = "modified wug";
        gitlet("commit", commitMessage3);

        String logContent = gitlet("log");
        assertArrayEquals(new String[] { commitMessage3, commitMessage2, commitMessage1 },
                extractCommitMessages(logContent));
    }

    @Test
    public void testBasicRemoveBranch() {
        gitlet("init");
        gitlet("branch", "dev");
        String stdout;
        stdout = gitlet("status");
        assertTrue(stdout.contains("dev"));
        gitlet("rm-branch", "dev");
        stdout = gitlet("status");
        assertTrue(!stdout.contains("dev"));
    }

    @Test
    public void testRemoveBranchFailure() {
        gitlet("init");
        String stdout;

        // Try to remove the branch you're currently on
        stdout = gitlet("rm-branch", "master");
        assertTrue(stdout.contains("Cannot remove the current branch."));

        // Try to remove not existed branch
        stdout =  gitlet("rm-branch", "dev");
        assertTrue(stdout.contains("A branch with that name does not exist."));

    }

    @Test
    public void testResetFailure() {
        gitlet("init");
        String stdout;
        stdout = gitlet("reset", "1234");
        assertTrue(stdout.contains("No commit with that id exists."));
    }

    @Test
    public void testResetBasic() {
        gitlet("init");
        gitlet("branch", "dev");
        gitlet("checkout", "dev");
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("init");
        gitlet("add", wugFileName);
        gitlet("commit", "added wug");
        String cid = Branches.loadBranches().getCurrentBranch().getHead().getCommitId();
        gitlet("checkout", "master");
        String stdout;
        stdout = gitlet("log");
        assertTrue(!stdout.contains(cid));
        gitlet("reset", cid);
        stdout = gitlet("log");
        assertTrue(stdout.contains(cid));
    }

    @Test
    public void testMergeFailure() {
        gitlet("init");
        String stdout;
        stdout = gitlet("merge", "master");
        assertTrue(stdout.contains("Cannot merge a branch with itself."));
        stdout = gitlet("merge", "dev");
        assertTrue(stdout.contains("A branch with that name does not exist."));
    }

    @Test
    public void testMergeBasic() {
        gitlet("init");
        gitlet("branch", "dev");
        String stdout;

        // Create a commit in master branch
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        gitlet("commit", "added wug");

        String modifiedText = "This is modified text.";
        writeFile(wugFileName, modifiedText);
        gitlet("add", wugFileName);
        gitlet("commit", "modify wug");


        gitlet("checkout", "dev");
        stdout = gitlet("merge", "master");
        assertTrue(stdout.contains("yes/no"));

        assertEquals(modifiedText, getText(wugFileName));
    }

    @Test
    public void testMergeConflict() {
        gitlet("init");
        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is the original wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        gitlet("commit", "added wug");

        gitlet("branch", "dev");

        String wugTextInMaster = "This is master version of wug.";
        writeFile(wugFileName, wugTextInMaster);
        gitlet("add", wugFileName);
        gitlet("commit", "modify wug in master");

        gitlet("checkout", "dev");
        String wugTextInDev = "This is dev version of wug.";
        writeFile(wugFileName, wugTextInDev);
        gitlet("add", wugFileName);
        gitlet("commit", "modify wug in dev");

        gitlet("merge", "master");
        assertEquals(wugTextInDev, getText(wugFileName));
        assertEquals(wugTextInMaster, getText(wugFileName+".conflicted"));
    }

    @Test
    public void testRebaseFailure() {
        gitlet("init");
        String stdout;
        stdout = gitlet("rebase", "master");
        assertTrue(stdout.contains("Cannot rebase a branch onto itself."));

        stdout = gitlet("rebase", "dev");
        assertTrue(stdout.contains("A branch with that name does not exist."));

        gitlet("branch", "dev");
        stdout = gitlet("rebase", "dev");
        assertTrue(stdout.contains("Already up-to-date."));
    }

    @Test
    public void testRebaseBasic() {
        gitlet("init");

        // If the current branch is in the history of the given branch, rebase
        // just moves the current branch to point to the same commit that the
        // given branch points to. No commits are replayed in this case.
        gitlet("branch", "dev");
        String commitMessage1 = "initial commit";

        String wugFileName = TESTING_DIR + "wug.txt";
        String wugText = "This is a wug.";
        createFile(wugFileName, wugText);
        gitlet("add", wugFileName);
        String commitMessage2 = "added wug";
        gitlet("commit", commitMessage2);

        writeFile(wugFileName, "modified");
        String stdout = gitlet("add", wugFileName);
        String commitMessage3 = "modified wug";
        gitlet("commit", commitMessage3);

        String logContent = gitlet("log");
        assertArrayEquals(new String[] { commitMessage3, commitMessage2, commitMessage1 },
                extractCommitMessages(logContent));

        gitlet("checkout", "dev");
        gitlet("rebase", "master");
        logContent = gitlet("log");
        assertArrayEquals(new String[] { commitMessage3, commitMessage2, commitMessage1 },
                extractCommitMessages(logContent));

        // Check normal rebase situation
        writeFile(wugFileName, "this is edited in dev branch.");
        gitlet("add", wugFileName);
        String commitMessage4 = "modified wug in dev branch";
        gitlet("commit", commitMessage4);

        gitlet("checkout", "master");
        writeFile(wugFileName, "this is edited in master branch.");
        gitlet("add", wugFileName);
        String commitMessage5 = "modified wug in master branch";
        gitlet("commit", commitMessage5);

        stdout = gitlet("rebase", "dev");
        assertEquals("this is edited in master branch.", getText(wugFileName));
        assertEquals("this is edited in dev branch.", getText(wugFileName+".conflicted"));
        logContent = gitlet("log");
        assertArrayEquals(new String[] { commitMessage5, commitMessage4, commitMessage3, commitMessage2, commitMessage1 },
                extractCommitMessages(logContent));
    }

    public static void main(String[] args) {
        jh61b.junit.textui.runClasses(GitletPublicTest.class);
    }

    /**
     * Convenience method for calling Gitlet's main. Anything that is printed
     * out during this call to main will NOT actually be printed out, but will
     * instead be returned as a string from this method.
     *
     * Prepares a 'yes' answer on System.in so as to automatically pass through
     * dangerous commands.
     *
     * The '...' syntax allows you to pass in an arbitrary number of String
     * arguments, which are packaged into a String[].
     */
    private static String gitlet(String... args) {
        PrintStream originalOut = System.out;
        InputStream originalIn = System.in;
        ByteArrayOutputStream printingResults = new ByteArrayOutputStream();
        try {
            /*
             * Below we change System.out, so that when you call
             * System.out.println(), it won't print to the screen, but will
             * instead be added to the printingResults object.
             */
            System.setOut(new PrintStream(printingResults));

            /*
             * Prepares the answer "yes" on System.In, to pretend as if a user
             * will type "yes". You won't be able to take user input during this
             * time.
             */
            String answer = "yes";
            InputStream is = new ByteArrayInputStream(answer.getBytes());
            System.setIn(is);

            /* Calls the main method using the input arguments. */
            Gitlet.main(args);

        } finally {
            /*
             * Restores System.out and System.in (So you can print normally and
             * take user input normally again).
             */
            System.setOut(originalOut);
            System.setIn(originalIn);
        }
        return printingResults.toString();
    }

    /**
     * Returns the text from a standard text file (won't work with special
     * characters).
     */
    private static String getText(String fileName) {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(fileName));
            return new String(encoded, StandardCharsets.UTF_8);
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * Creates a new file with the given fileName and gives it the text
     * fileText.
     */
    private static void createFile(String fileName, String fileText) {
        File f = new File(fileName);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        writeFile(fileName, fileText);
    }

    /**
     * Replaces all text in the existing file with the given text.
     */
    private static void writeFile(String fileName, String fileText) {
        FileWriter fw = null;
        try {
            File f = new File(fileName);
            fw = new FileWriter(f, false);
            fw.write(fileText);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Deletes the file and all files inside it, if it is a directory.
     */
    private static void recursiveDelete(File d) {
        if (d.isDirectory()) {
            for (File f : d.listFiles()) {
                recursiveDelete(f);
            }
        }
        d.delete();
    }

    /**
     * Returns an array of commit messages associated with what log has printed
     * out.
     */
    private static String[] extractCommitMessages(String logOutput) {
        String[] logChunks = logOutput.split("====");
        int numMessages = logChunks.length - 1;
        String[] messages = new String[numMessages];
        for (int i = 0; i < numMessages; i++) {
            // System.out.println(logChunks[i + 1]);
            String[] logLines = logChunks[i + 1].split(LINE_SEPARATOR);
            messages[i] = logLines[3];
        }
        return messages;
    }
}
