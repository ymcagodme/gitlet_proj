import java.io.*;
import java.text.SimpleDateFormat;
import java.security.MessageDigest;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Commit implements Serializable {

    private static final long serialVersionUID = 123456789L;
    private String parentId;
    private boolean isCommitted;
    private String commitMsg;
    private HashSet<String> stagedFiles;
    private HashSet<String> trackingFiles;
    private HashSet<String> removalFiles;
    private Long timeStamp; // the current time in milliseconds
    private String _commitId;

    public Commit(String parentId) {
        this.parentId = parentId;
        stagedFiles = new HashSet<String>();
        trackingFiles = new HashSet<String>();
        removalFiles = new HashSet<String>();

        if (parentId == null) {
            this.parentId = "-1";
        } else {
            this.parentId = parentId;
            Commit parent = Commit.loadCommit(parentId);
            HashSet<String> pTrackingFiles = parent.getTrackingFiles();
            HashSet<String> pStagedFiles = parent.getStagedFiles();
            trackingFiles = new HashSet<String>(pTrackingFiles);
            for (String fn : pStagedFiles) {
                trackingFiles.add(fn);
            }
        }

        isCommitted = false;
        commitMsg = "";
        timeStamp = new Long(System.currentTimeMillis());
    }

    public void commit(String commitMsg) {
        if (!isCommitted) {
            String oldId = getCommitId();
            timeStamp = new Long(System.currentTimeMillis());
            setCommitMsg(commitMsg);
            _commitId = getCommitId();
            isCommitted = true;
            this.save();

            String oldPath = GitletUtils.GITLET_DIR + "commits/" + oldId + "/";
            GitletUtils.recursiveDelete(new File(oldPath));

            Commits commits = Commits.getInstance();
            commits.add(this);
            commits.save();
        }
    }

    public void removeFile(String fileName) throws Exception {
        if (stagedFiles.contains(fileName)) {
            stagedFiles.remove(fileName);
        } else if (trackingFiles.contains(fileName)) {
            trackingFiles.remove(fileName);
            removalFiles.add(fileName);
        } else {
            throw new Exception("No files to be removed");
        }
    }

    public Set<String> getRemovalFiles() {
        return removalFiles;
    }

    public HashSet<String> getTrackingFiles() {
        return trackingFiles;
    }

    public boolean isCommitted() {
        return isCommitted;
    }

    public String getDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(timeStamp);
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public Commit getParent() {
        return loadCommit(parentId);
    }

    public String getCommitMsg() {
        return commitMsg;
    }

    private void setCommitMsg(String commitMsg) {
        this.commitMsg = commitMsg;
    }

    public void setParentId(String commitId) {
        String oldPath = GitletUtils.GITLET_DIR + "commits/" + getCommitId() + "/";
        parentId = commitId;
        this.save();
        GitletUtils.recursiveDelete(new File(oldPath));
    }

    // Inspired from:
    // stackoverflow.com/questions/4400774/java-calculate-a-sha1-of-a-string
    public String getCommitId() {
        if (isCommitted) {
            return _commitId;
        }
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.reset();
            String input = parentId + commitMsg + getTimeStamp();
            md.update(input.getBytes());
            return new BigInteger(1, md.digest()).toString(16);
        }
        catch (Exception e) {
            e.printStackTrace();
            return "Error while generating commit ID";
        }
    }

    public HashSet<String> getStagedFiles() {
        return stagedFiles;
    }

    public void addFile(String fileName) {
        if (!isCommitted) {
            if (removalFiles.contains(fileName)) {
                removalFiles.remove(fileName);
            } else {
                stagedFiles.add(fileName);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Commit " + getCommitId() + ".\n");
        sb.append(getDateTime() + "\n");
        sb.append(getCommitMsg());
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Commit) {
            Commit other = (Commit) o;
            if (getCommitId().equals(other.getCommitId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        BigInteger bi = new BigInteger(getCommitId(), 16);
        return bi.intValue();
    }

    /**
     * ========== Utils ===========
    */
    public void save() {
        Commit commit = this;
        try {
            String commitId = commit.getCommitId();
            String GITLET_DIR = GitletUtils.GITLET_DIR;
            String commitPath = GITLET_DIR + "commits/" + commitId + "/";
            File commitFile = new File(commitPath);
            if (!commitFile.exists()) {
                commitFile.mkdirs();
            }
            commitPath += "commit.ser";
            commitFile = new File(commitPath);
            commitFile.createNewFile();
            FileOutputStream fileOut = new FileOutputStream(commitFile);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(commit);
        } catch (IOException e) {
            String msg = "IOException while saving Commit.";
            System.out.println(msg);
        }
    }

    public static Commit loadCommit(String commitId) {
        Commit myCommit = null;
        String GITLET_DIR = GitletUtils.GITLET_DIR;
        String commitPath = GITLET_DIR + "commits/" + commitId
                          + "/commit.ser";
        File commitFile = new File(commitPath);
        if (commitFile.exists()) {
            try {
                FileInputStream fileIn = new FileInputStream(commitFile);
                ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                myCommit = (Commit) objectIn.readObject();
            } catch (IOException e) {
                String msg = "IOException while loading Commit.";
                System.out.println(msg);
            } catch (ClassNotFoundException e) {
                String msg = "ClassNotFoundException while loading Commit.";
                System.out.println(msg);
            }
        }
        return myCommit;
    }
}
