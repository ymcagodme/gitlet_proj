import java.io.*;
import java.util.Set;
import java.util.HashSet;

public class Commits implements Serializable {
    private static final long serialVersionUID = 123456789L; // dummy UID
    private static Commits instance; // Apply Singleton Design Pattern

    private Set<String> commits; // {commit_ids}

    private Commits() {
        commits = new HashSet<String>();
        this.save();
    }

    public static Commits getInstance() {
        if (instance == null && (instance = loadCommits()) == null) {
            instance = new Commits();
        }

        return instance;
    }

    public Set<Commit> all() {
        Set<Commit> allCommits = new HashSet<Commit>();
        for (String commitId : commits) {
            allCommits.add(Commit.loadCommit(commitId));
        }
        return allCommits;
    }

    public Commit find(String commitId) {
        if (commits.contains(commitId)) {
            return Commit.loadCommit(commitId);
        }
        return null;
    }

    public void add(Commit c) {
        commits.add(c.getCommitId());
    }

    public void remove(Commit c) {
        commits.remove(c.getCommitId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("=====================\n");
        sb.append("====== Commits ======\n");
        sb.append("=====================\n");
        sb.append("All Commits: \n");
        for (Commit c : all()) {
            sb.append(c.toString());
        }
        return sb.toString();
    }

    /**
     * -------------- Utility Methods -------------------
     */
    public void save() {
        try {
            String GITLET_DIR = GitletUtils.GITLET_DIR;
            String commitsPath = GITLET_DIR + "commits.ser";
            File commitsFile = new File(commitsPath);
            FileOutputStream fileOut = new FileOutputStream(commitsFile);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(this);
        } catch (IOException e) {
            String msg = "IOException while saving Branches.";
            System.out.println(msg);
        }
    }

    protected static Commits loadCommits() {
        Commits myCommits = null;
        String GITLET_DIR = GitletUtils.GITLET_DIR;
        String commitsPath = GITLET_DIR + "commits.ser";
        File commitsFile = new File(commitsPath);
        if (commitsFile.exists()) {
            try {
                FileInputStream fileIn = new FileInputStream(commitsFile);
                ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                myCommits = (Commits) objectIn.readObject();
            } catch (IOException e) {
                String msg = "IOException while loading Branches.";
                System.out.println(msg);
            } catch (ClassNotFoundException e) {
                String msg = "ClassNotFoundException while loading Branches.";
                System.out.println(msg);
            }
        }
        return myCommits;
    }
}
